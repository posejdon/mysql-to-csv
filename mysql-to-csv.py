#!/usr/bin/python3

import mysql.connector
import csv
import io

cnx = mysql.connector.connect(user='USERNAME', password='PASSWORD',
                              host='HOST',
                              database='DATABASE')
cursor = cnx.cursor()

fields = ('FIELDS', 'TO', 'EXPORT')

query = """
SELECT {} FROM table
""".format(','.join(fields))

cursor.execute(query)

with io.open('OUTPUT-FILE.csv','w') as out:
	csv_out=csv.writer(out, delimiter='\t', escapechar='"', quoting=csv.QUOTE_ALL)
	csv_out.writerow(fields)
	for row in cursor:
		csv_out.writerow(row)

cnx.close() 
